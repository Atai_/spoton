import React, {useState, useEffect} from 'react';
import { SafeAreaView, StyleSheet, Text, View, Image, TextInput, TouchableOpacity, TouchableWithoutFeedback } from 'react-native';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';

const closeBtn = require('../assets/Close_button.png');
const pin = require('../assets/Maps_Icon.png');

export default function Location ( {navigation} ) {

    const [state, setState] = useState({
        latitude: 37.78825,
        longitude: -122.4324,
        track_id: "5e444be237b4488cbbdd90f938961f5e",
    })

    const onStart = () => {
        const data = startTrack();
        setState({track_id: data.track_id});
      };

    const startTrack = async () => {
        const response = await fetch('http://test-spotapp.ru:8080/start_track', {
          method: 'post',
        });
        const json = await response.json();
        return json;
      };

  return (
    <SafeAreaView style={styles.body}>
        <TouchableWithoutFeedback onPress={() => {navigation.navigate('Home')}}>
            <Image   style={styles.closebtn} 
                source={closeBtn}
            />
        </TouchableWithoutFeedback>
        <View style={styles.container}>
            <View style={styles.circle}>
                <Image style={styles.pin} 
                    source={pin}
                />
                <Text style={styles.loc}>
                    MyLoc
                </Text>
            </View>
        </View>
        <View style={{marginTop:150}}>
            <View >
                <Text style={{fontSize: 30, color:'#707070', fontWeight: 'bold', marginBottom: 21}}>
                    Выберите вашу геопозицию
                </Text>
                <View style={styles.map_container}>
                    <MapView
                        initialRegion={{
                            latitude: 37.78825,
                            longitude: -122.4324,
                            latitudeDelta: 0.002,
                            longitudeDelta: 0.002,
                        }}
                        onPress={(event) => setState({latitude: event.nativeEvent.coordinate.latitude, longitude: event.nativeEvent.coordinate.longitude})}
                        style={styles.map}>
                            <Marker coordinate={{latitude: state.latitude, longitude: state.longitude}}>
                                <Image source={pin} />
                            </Marker>
                    </MapView>

                    <TouchableOpacity onPress={onStart}>
                        <Text
                        style={
                                {
                                    textAlign:'center', 
                                    marginTop: 20, 
                                    fontSize:18, 
                                    fontWeight:'bold', 
                                    color: '#00A092'
                                }
                            } 
                        >Создать трек</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <View style={{marginTop: 20}}>  
                <Text style={styles.btn} onPress={() => navigation.navigate('SendLocation', {  data: state  })}>
                    Выбрать
                </Text>
            </View>
        </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
    body: {
        margin: 20,
        marginBottom: 0,
        marginTop: 0
    },
    container: {
        flex: 1,
        alignItems: 'center',
        marginTop: 41,

    },
    map_container: {
      height: 280,
      width: '100%',
      justifyContent: 'flex-end',
      alignItems: 'center',
      marginTop: 20
    },
    map: {
        width: '100%',
        height:'100%'
    },
    closebtn:{
        position: 'absolute',
        right: 0,
        top: 40,
        width:50,
        height: 50,
        zIndex:111
    },
    circle:{
        width: 108,
        height:108,
        borderRadius: 90,

        backgroundColor: "#ECF0F3",

        shadowColor: "black",
        shadowOpacity: 0.15,
        shadowOffset: { width: 5, height: 8},
        shadowRadius: 50,

        alignItems: 'center',
    },
    loc: {
        fontSize: 16,
        fontWeight: 'bold',
        color:'#797979'
    },
    input:{
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 20,
        padding: 10,
        paddingLeft: 20,
        marginBottom: 20,
        fontSize: 16
    },
    pin:{
        width: 30,
        height:57,
        marginTop:10,
        marginBottom: 7
    },
    btn:{
        width: '100%',
        height: 50,
        padding: 13,
        color: '#707070',
        borderColor: "#E2E2E2",
        borderWidth: 1,
        borderRadius: 25,
        textAlign: 'center',
        shadowColor: '#97A7C380',
        shadowRadius: 30,
        shadowOpacity: 1,
    }
});
