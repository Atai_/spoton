import React, { useState, useEffect } from 'react';
import { SafeAreaView, StyleSheet, Text, View, Image, TouchableWithoutFeedback ,TextContainer } from 'react-native';

const backBtn = require('../assets/Back_button.png');
const pin = require('../assets/Maps_Icon.png');

export default function AcceptLocation ( {navigation, route} ) {

    // const data = route.params
    // console.log(data)

    const [state, setState] = useState({
        latitude: 37.78825,
        longitude: -122.4324,
        order_id: ''
    });
    
    const getTrackList = async () => {
        const response = await fetch(
            `http://test-spotapp.ru:8080/get_track/5e444be237b4488cbbdd90f938961f5e`
        //   `http://test-spotapp.ru:8080/get_track/${data.trackId}`,
        );
        const json = await response.json();
        return json;
      };

      useEffect(() => {
        getTrackList()
        .then((id) => {
          setState({order_id: id});
        });
      }, []);

    

  return (
    <SafeAreaView style={styles.body}>
        <TouchableWithoutFeedback onPress={() => navigation.goBack()}>
            <Image style={styles.backBtn} 
                source={backBtn}
            />
        </TouchableWithoutFeedback>
        <View style={styles.container}>
            <View style={styles.circle}>
                <Image style={styles.pin} 
                    source={pin}
                />
                <Text style={styles.loc}>
                    MyLoc
                </Text>
            </View>
        </View>
        <View  style={{marginTop:140}}>
            <View >
                <Text style={{fontSize: 30, color:'#707070', fontWeight: 'bold', marginBottom: 21}}>
                    Проверьте добавленные точки
                </Text>
                <View style={{flexDirection:'row', justifyContent: 'space-between', alignItems: 'center'}}>
                    <View style={{alignItems: 'center'}}>
                        <TextContainer style={styles.input} text={state.latitude.toFixed(4)} />
                        <Text style={{fontSize: 12, fontWeight: 'bold'}}>Широта</Text>
                    </View>
                    <View style={{alignItems: 'center'}}>
                        <TextContainer style={styles.input} text={state.longitude.toFixed(4)} />
                        <Text style={{fontSize: 12, fontWeight: 'bold'}}>Долгота</Text>
                    </View>
                </View>
            </View>

            <View>
                <Text style={{fontSize: 20, color:'#707070', fontWeight: 'bold', marginBottom: 19, marginTop: 31}}>Выберите добавленную точку</Text>
                {/* {state?.map((item) => {
                    <TouchableWithoutFeedback onPress={() => setSelectedTrack(item)} text={`Точка ${item.order_id + 1}`}></TouchableWithoutFeedback>
                })} */}
            </View>

            <View style={{marginTop: 145}}>  
                <Text style={styles.btn} onPress={() => navigation.navigate('Location')}>
                    Добавить еще
                </Text>
            </View>
        </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
    body: {
        margin: 20,
        marginBottom: 0,
        marginTop: 0
    },
    container: {
        flex: 1,
        alignItems: 'center',
        marginTop: 41,

    },
    backBtn:{
        position: 'absolute',
        right: 0,
        top: 40,
        width: 50,
        height: 50,
    },
    circle:{
        width: 108,
        height:108,
        borderRadius: 90,

        backgroundColor: "#ECF0F3",

        shadowColor: "black",
        shadowOpacity: 0.15,
        shadowOffset: { width: 5, height: 8},
        shadowRadius: 50,

        alignItems: 'center',
    },
    loc: {
        fontSize: 16,
        fontWeight: 'bold',
        color:'#797979'
    },
    pin:{
        width: 30,
        height:57,
        marginTop:10,
        marginBottom: 7
    },
    input:{
        width: 158,
        height: 50,
        textAlign: 'center'
    },
    btn:{
        width: '100%',
        height: 50,
        padding: 13,
        color: '#707070',
        borderColor: "#E2E2E2",
        borderWidth: 1,
        borderRadius: 25,
        textAlign: 'center',
        shadowColor: '#97A7C380',
        shadowRadius: 30,
        shadowOpacity: 1,
    }
});