import React from 'react';
import { StyleSheet, SafeAreaView, Text, Pressable } from 'react-native';

export default function Begin ({navigation}) {
  return (
    <SafeAreaView style={styles.block}>
      <Pressable style={styles.btn}>
        <Text style={styles.text} onPress={() => {navigation.push('Location')}}>
          Начать
        </Text>
      </Pressable>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  block:{
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btn:{
      backgroundColor: "#ECF0F3",

      shadowColor: "black",
      borderRadius: 12,
      shadowOpacity: 0.1,
      shadowOffset: { width: 3, height: 5},
      shadowRadius: 12,
      elevation:7,

      width: 106,
      height:106,
      alignItems: 'center',
      justifyContent: 'center',
  },
  text:{
    fontSize: 20,
    fontWeight: 'bold',
    color: '#707070',
  }
});
