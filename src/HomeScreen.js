import React from 'react';
import { SafeAreaView, StyleSheet, Text } from 'react-native';

export default function HomeScreen ( {navigation} ) {
  return (
    <SafeAreaView style={{flex: 1, marginTop: 40}}>
      <Text style={styles.block} onPress={() => navigation.push('Begin')}>
        Нажмите на экран
      </Text>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  block:{
    fontSize: 20,
    fontWeight: 'bold',
    color: '#707070',

    paddingBottom: 56,
    textAlign:"center",
    textAlignVertical: 'bottom',
    
    width: "100%",
    height: "100%",
  }
});
