import React, { useState, useEffect } from 'react';
import { SafeAreaView, StyleSheet, Text, View, Image, TouchableWithoutFeedback } from 'react-native';

const backBtn = require('../assets/Back_button.png');
const pin = require('../assets/Maps_Icon.png');

export default function Location ( {navigation, route} ) {

  const location = route.params.data

  console.log(location)

  const sendLocation = () => {
    fetch('http://test-spotapp.ru:8080/save_track_point', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            latitude: location.latitude,
            longitude: location.longitude,
            track_id: location.track_id
        })
    });
    navigation.navigate('Accept', {track_id: location.track_id})
  }

  return (
    <SafeAreaView style={styles.body}>
        <View style={styles.container}>
            <TouchableWithoutFeedback style={{width: '100%', zIndex: 1111}}
            onPress={() => {navigation.goBack()}}>
                <Image 
                    
                    style={styles.backBtn} 
                    source={backBtn}
                />
            </TouchableWithoutFeedback>
            <View style={styles.circle}>
                <Image style={styles.pin} 
                    source={pin}
                />
                <Text style={styles.loc}>
                    MyLoc
                </Text>
            </View>
        </View>
        <View style={{marginTop:140}}>
            <View >
                <Text style={{fontSize: 28, color:'#707070', fontWeight: 'bold', marginTop: 50, marginBottom: 80}}>
                    Отправьте выбранную геопозицию
                </Text>
                <View style={{justifyContent: 'space-between', alignItems: 'center'}}>
                    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent:'space-between', width:'90%'}}>
                        <Text style={{fontSize: 12, fontWeight: 'bold', color:'#707070'}}>Широта</Text>
                        <Text style={styles.input}>{location.latitude}</Text>
                    </View>
                    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent:'space-between', width:'90%', marginTop: 40}}>
                        <Text style={{fontSize: 12, fontWeight: 'bold', color:'#707070'}}>Долгота</Text>
                        <Text style={styles.input}>{location.longitude}</Text>
                    </View>
                </View>
            </View>
            <View style={{marginTop: 80}} >  
                <Text style={styles.btn} onPress={sendLocation}>
                    Отправить
                </Text>
            </View>
        </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
    body: {
        margin: 20,
        marginBottom: 0,
        marginTop: 0,
        position:'relative'
    },
    container: {
        flex: 1,
        alignItems: 'center',
        marginTop: 50,
    },
    backBtn:{
        position: 'absolute',
        top: 20,
        left: 0,
        width: 50,
        height: 50,
        zIndex:111
    },
    circle:{
        width: 108,
        height:108,
        borderRadius: 90,

        backgroundColor: "#ECF0F3",

        shadowColor: "black",
        shadowOpacity: 0.15,
        shadowOffset: { width: 5, height: 8},
        shadowRadius: 50,

        alignItems: 'center',
    },
    loc: {
        fontSize: 16,
        fontWeight: 'bold',
        color:'#797979'
    },
    pin:{
        width: 30,
        height:57,
        marginTop:10,
        marginBottom: 7
    },
    input:{
        width: 158,
        height: 50,
        textAlignVertical: 'center',
        textAlign: 'center',
        backgroundColor: '#ECF0F3',
        borderColor: 'white',
        borderWidth: 2,
        borderRadius: 30,
        shadowColor: '#97A7C380',
        shadowRadius: 30,
        shadowOpacity: 0.5,
    },
    btn:{
        width: '100%',
        height: 50,
        padding: 13,
        color: '#707070',
        borderColor: "#97A7C380",
        borderWidth: 1,
        borderRadius: 25,
        textAlign: 'center',
        shadowColor: '#97A7C380',
        shadowRadius: 30,
        shadowOpacity: 1,
    }
});
