import React from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import HomeScreen from './src/HomeScreen.js';
import Begin from './src/Begin';
import Location from './src/Location';
import AcceptLocation from './src/AcceptLocation';
import SendLocation from './src/SendLocation';

const Stack = createStackNavigator();

export default function Navigate() {
  return <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen 
          name="Begin" 
          component={Begin} 
          options={{ headerShown: false }}
        />
        <Stack.Screen 
          name="Location" 
          component={Location} 
          options={{ headerShown: false }}
        /> 
      <Stack.Screen 
          name="SendLocation" 
          component={SendLocation} 
          options={{ headerShown: false }}
        />
        <Stack.Screen 
          name="Accept" 
          component={AcceptLocation} 
          options={{ headerShown: false }}
        />
      </Stack.Navigator>
    </NavigationContainer>;
};